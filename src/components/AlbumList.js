import React, {Component} from 'react';
import {Text, ScrollView} from 'react-native';
import axios from 'axios';
import AlbumDetail from './AlbumDetail';

const url = 'https://rallycoding.herokuapp.com/api/music_albums';

class AlbumList extends Component {
    constructor(props) {
        super(props);
        this.renderAlbums = this.renderAlbums.bind(this);
        this.state = {
            albums: []
        }
    }

    componentWillMount() {
        axios.get(url).then(response => {
            this.setState({
                albums: response.data
            });
        });
    }

    renderAlbums() {
        return this.state.albums.map(album => <AlbumDetail key={album.title} album={album}/>)
    }
    render() {
        return (
            <ScrollView>
                {this.renderAlbums()}
            </ScrollView>
        );
    };
}

export default AlbumList;