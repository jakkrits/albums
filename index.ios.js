//1. import a library to help create a component
//2. create a component
//3. render it to the device

import React, {Component} from 'react';
import { View, AppRegistry} from 'react-native';
import Header from './src/components/Header';
import AlbumList from './src/components/AlbumList';

const App = () => {
  props = {
    headerText: 'Album!'
  };
  return (
      <View style={{flex: 1}}>
        <Header headerText={props.headerText}/>
        <AlbumList/>
      </View>

  );
};

AppRegistry.registerComponent('albums', () => App);